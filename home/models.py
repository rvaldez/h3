from __future__ import absolute_import, unicode_literals

from django.db import models

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

class Inicio(Page):
    body = RichTextField(blank=True)

    def get_context(self, request):
    # Update context to include only published posts, ordered by reverse-chron
        context = super(Inicio, self).get_context(request)
        tipopages = self.get_children().live().order_by('-first_published_at')
        context['tipopages'] = tipopages
        return context

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]

