from __future__ import absolute_import, unicode_literals

from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True



# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0n7z=(g4k(#sy1t85hf$6#00guz!9w82k*15xs1vy@be0!@vo6'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


try:
    from .local import *
except ImportError:
    pass
