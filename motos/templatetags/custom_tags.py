from django import template
register = template.Library()

from ..models import DetalleDeVehiculo

@register.simple_tag
def any_function():
	return DetalleDeVehiculo.objects.count()

@register.assignment_tag(takes_context=True)
def carga_promos(context):
    primero = context['request'].site.root_page.get_children().live().in_menu()
    segundo = context['request'].site.root_page.get_children().live()
    return segundo